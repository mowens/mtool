package main

import (
	"fmt"
	"gitee.com/mowens/mtool/data/country"
	"gitee.com/mowens/mtool/data/valuta"
)

func main() {
	//提取国家地区语种货币符号等信息
	t, ok := country.GetCountryInfo("US", "New")
	if ok {
		fmt.Printf("%+v\n", t)
	}
	list := country.GetCountryListInfo("CN")
	//list := country.GetCountryTimeZoneList("CN")
	for k, v := range list {
		fmt.Printf("%v\t%v\n", k, v)
	}
	fmt.Println()
	////获取汇率计算
	valuta.LoadCountryInfo("")
	myr := valuta.GetCurrencyExchangeRate("usd")
	fmt.Printf("%+v\n", myr)
	us := valuta.GetCountryExchangeRate("us")
	fmt.Printf("%+v\n", us)
	cur, name := valuta.ExchangeCountryValuta(100, "US")
	fmt.Printf("人民币 兑换 货币：%s\t数量%f\n", name, cur)
	cny, cname := valuta.ExchangeCountryValutaTo("CN", 100)
	fmt.Printf("US币 兑换 货币：%s\t数量:%f\n", cname, cny)
}
