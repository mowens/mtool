package country

import (
	"encoding/json"
	"log"
	"os"
	"strings"
)

type CountryLangZone struct {
	Id              int    `json:"id"`
	ZhCountry       string `json:"zh_country"`
	LangCountry     string `json:"lang_country"`
	Country         string `json:"country"`
	Code2           string `json:"code2"`
	Code3           string `json:"code3"`
	Language        string `json:"language"`
	LangCode        string `json:"lang_code"`
	TimeZone        string `json:"time_zone"`
	ZoneCity        string `json:"zone_city"`
	GmtOffset       string `json:"gmt_offset"`
	ZoneOffset      int    `json:"zone_offset"`
	ZonePrior       int    `json:"zone_prior"`
	CurrencySymbol  string `json:"currency_symbol"`
	CurrencyCode    string `json:"currency_code"`
	CurrencyNumeric int    `json:"currency_numeric"`
	CurrencyName    string `json:"currency_name"`
	ZhCurrencyName  string `json:"zh_currency_name"`
}

var clzMap = make(map[string]map[string]CountryLangZone)

func init() {
	//打开已保存的JSON文件
	clz, err := os.Open("data/country/country_lang_zone.json")
	if err != nil {
		log.Fatalf("打开地理时区语种配置文件出错：%s\n", err.Error())
	}
	defer clz.Close()
	var cbuf []CountryLangZone
	temp := json.NewDecoder(clz)
	err = temp.Decode(&cbuf)
	if err != nil {
		log.Fatalf("读取地理时区语种配置文件出错：%s\n", err.Error())
	}
	for k := range cbuf {
		if clzMap[cbuf[k].Code2] == nil {
			clzMap[cbuf[k].Code2] = map[string]CountryLangZone{}
		}
		clzMap[strings.ToUpper(cbuf[k].Code2)][strings.ToLower(cbuf[k].ZoneCity)] = cbuf[k]
	}
}

// GetCountryInfo 获取国家信息
// code 国家国家编码
// city 可选参数 [时区地区]名称 英语一下划线分割
func GetCountryInfo(code string, city ...string) (CountryLangZone, bool) {
	temp := clzMap[strings.ToUpper(code)]
	for k := range temp {
		if len(city) == 0 {
			return temp[k], true
		} else {
			return temp[strings.ToLower(city[0])], true
		}
	}
	return CountryLangZone{}, false
}

// GetCountryListInfo 获取国家时区地区信息集合
// code 国家地区简称
func GetCountryListInfo(code string) map[string]CountryLangZone {
	return clzMap[strings.ToUpper(code)]
}

// GetCountryTimeZoneList 获取指定国家所在地区的包含的时区集合
// code 国家地区简称
func GetCountryTimeZoneList(code string) []string {
	temp := clzMap[strings.ToUpper(code)]
	var list []string
	for k := range temp {
		list = append(list, temp[k].ZoneCity)
	}
	return list
}
